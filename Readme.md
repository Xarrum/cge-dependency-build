## Build libRocket ##
*Note: building librocket using these steps only works on linux.*

1. Download FreeType2: https://sourceforge.net/projects/freetype/files/freetype2/2.6.5/
2. Download libRocket: https://github.com/libRocket/libRocket
3. Download the dependency build scripts: https://bitbucket.org/Xarrum/cge-dependency-build
3. Set the following environment variables:
	* NDK\_ROOT to point to the ndk root directory. Note that you should use NDK version r10e (see Build Anax above).
	* FREETYPE\_SOURCE\_DIR to the freetype source directory (e.g. FREETYPE\_SOURCE\_DIR=/usr/local/src/freetype-2.6.5)
	* LIBROCKET\_SOURCE\_DIR to the libRocket source directory
4. Move to the directory where you put the dependency build scripts and run the script:
	```
	§ ./build
	```
5. libRocket should now compile and the resulting libs packed into a librocket.tar.gz archive located in the same directory as the build script.
6. Unzip the the librocket archive into the /cge/Dependencies directory.